/* #region Libraries */
#include <Arduino.h>
#include <Streaming.h>

// -- Clock ----
#include "DS3231.h"
#include "Time.h"
DS3231 rtc;
bool h12, PM;
RifTime t;
char buffer[24];

// -- Game Engine ----
#include <Mathf.h>
#include <PlayerPrefs.h>

// -- Controller ----
#include <Controller.h>
Controller controller;

// -- Sound ----
#include <Sound.h>
#include <Pitches.h>
#include <MiiChannelSong.h>
Sound sound;
MiiChannelSong music;

// -- OLED ----
#include <OLED.h>
#include <BitmapImages.h>
OLED display;

// -- TM1638 ----
#include "EBTM.h"
EBTM tm1638;

// -- Pong Game ----
#include <Field.h>
Field gameField(
    Vector2(0, display.BLUE_OFFSET),
    Vector2(display.WIDTH, display.HEIGHT));

#include <Paddle.h>
const int paddleSize = 12;

Paddle playerPaddle(
    Vector2(5, gameField.center.Y),
    Vector2(0, paddleSize),
    4);

Paddle computerPaddle(
    Vector2(display.WIDTH - 5, gameField.center.Y),
    Vector2(0, paddleSize),
    4);

#include "CPU.h"
CPU cpu(computerPaddle, 5);

#include <Ball.h>
Ball ball(
    gameField.center,
    Vector2(),
    2,
    3);
/* #endregion */

bool musicFlag = false; // Flag to enable the play of music

bool introScene = true, menuScene = false, difficultyScene = false, gameSetupScene = false, gameScene = false, endScene = false; // Flags for each Scene/Meny
int optionSelector = 0; // Helper Variable to select options on the Menus

//Game Variables
int playerScore = 0, computerScore = 0; // Score Tracking
bool playerWon = false; //Player or Cpu Won?
RifTime matchStart; // Match Time Tracking Helper

/* #region Menus Logic */

//Changes the and stores Menu Option being Selected
void changeSelection(int i, int size)
{
  optionSelector += i;

  //If out of range, go around
  if (optionSelector < 0) 
  {
    optionSelector = size - 1;
  }
  else if (optionSelector >= size)
  {
    optionSelector = 0;
  }
}

//Display the current real time on the clock (Should be GMT+0)
void showTime(){
  rtc.getTime(t); //Gets current Time
  String time = t.toString(buffer); //Converts DateTime to String
  tm1638.getModule().setDisplayToString("  " + time.substring(9,11) + "  " + time.substring(12,14)); //Solution to show on the 7-Seg Display The Current Time
}

//Displays The Intro scene
void displayIntro()
{
  //Clears any pending tasks
  sound.stop();
  tm1638.getModule().clearDisplay();

  //Starts intro Sequence
  String message = "Loading..";
  int messageToShow = 0;
  //The for Loop is used to Scroll down the Logo and Making the LEDS flash in intensity as well making each letter of the message appear on the 7-Seg (For flavor)
  for (int i = -64; i < 64; i += 4) //+= 4 is the speed of Animation
  {
    //LED Randomizer and LED/Display Intensity Randomizer
    tm1638.getModule().setupDisplay(true, random(0,7));
    tm1638.getModule().setLED(TM1638_COLOR_RED, random(0,8));
    tm1638.getModule().setLED(TM1638_COLOR_NONE, random(0,8));

    //Logic to show each letter and cycle the "animation" on the display
    tm1638.getModule().setDisplayToString(message.substring(0, messageToShow));
    messageToShow++;
    if(messageToShow > 9){messageToShow = 0; tm1638.getModule().clearDisplay();}
  
    //Image Scroll Down Animation
    display.clearScreen();
    display.drawBitmapImage(Vector2(0, i), Vector2(128, 64), pongLogo);
    display.renderScreen();

    delay(10); //Need a small delay to permit the display render everything.
  }

  //Updates the Flags for next Scene
  introScene = false;
  menuScene = true;
  optionSelector = 0;

  //Clears the LEDS/7-Segs to be used
  tm1638.getModule().setLEDs(0);
  tm1638.getModule().clearDisplay();
  tm1638.getModule().setupDisplay(true, 3);

  //Makes the music start from the beginning
  music.resetSong();
}

//Displays The Main Menu Scene
void displayMenu()
{

  showTime(); // Shows Current Time on 7-Seg display
  musicFlag = true; //Turn On Music

  //Writes Scene Tittle on the middle of the yeallow area
  String tittle = "MAIN MENU";
  display.writeText(
      tittle,
      Vector2(
          display.center.X - (tittle.length() * display.TEXT_SIZE / 2), Mathf::findCenter(0, display.BLUE_OFFSET - display.TEXT_SIZE)));

  //Writes the Menu Options on the middle of the blue area
  String options[] = {"Start Game", "Exit"};
  for (int i = 0; i < 2; i++)
  {
    String message = "";
    if (optionSelector == i)
    {
      message = "> " + options[i] + " <"; // Add visual Cue on the current option selected
    }
    else
    {
      message = options[i];
    }

    display.writeText(message,
                      Vector2(
                          display.center.X - (message.length() * display.TEXT_SIZE / 2),
                          (display.BLUE_OFFSET + display.TEXT_SIZE) * (i + 1)));
  }

  //Changes the menu option selected
  Vector2 analogInput = controller.getAnalogKey();
  if (analogInput.Y == 1)
  {
    sound.playSound(NOTE_G5);
    changeSelection(1, 2);
  }
  else if (analogInput.Y == -1)
  {
    sound.playSound(NOTE_G5);
    changeSelection(-1, 2);
  }

  //Confirm the selected option
  if (controller.getKey(KeyCode::A))
  {
    sound.playSound(NOTE_G5);
    switch (optionSelector)
    {
    case 0:
      //Start Difficulty Scene
      menuScene = false;
      difficultyScene = true;
      break;

    case 1:
      //Reloads Game
      menuScene = false;
      introScene = true;
    }
  }

  //Cancell or Reloads the Game
  if (controller.getKey(KeyCode::B))
  {
    sound.playSound(NOTE_G5);
    if (optionSelector == 1)
    {
      menuScene = false;
      introScene = true;
    }
    else
    {
      optionSelector = 1;
    }
  }

  delay(10); // Need delay so it wont have multiple button presses
}

//Displays The Difficulty Menu
void displayDiffucltyMenu()
{
  showTime(); // Shows Current Time on 7-Seg display

  //Menu Tittle
  String tittle = "DIFFICULTY LEVEL";
  display.writeText(
      tittle,
      Vector2(
          display.center.X - (tittle.length() * display.TEXT_SIZE / 2), Mathf::findCenter(0, display.BLUE_OFFSET - display.TEXT_SIZE)));

  //Menu Options
  String options[] = {"Easy", "Normal", "Hard"};
  String message = "";
  for (int i = 0; i < 3; i++)
  {
    if (optionSelector == i)
    {
      message += " >" + options[i] + "< ";
    }
    else
    {
      message += " " + options[i] + " ";
    }
  }

  //Writes the menu options after Logic
  display.writeText(message,
                    Vector2(
                        display.center.X - (message.length() * display.TEXT_SIZE / 2),
                        (display.BLUE_OFFSET + display.TEXT_SIZE)));

  String startMessage = "Press A to begin";
  display.writeText(startMessage,
                    Vector2(
                        display.center.X - (startMessage.length() * display.TEXT_SIZE / 2),
                        (display.BLUE_OFFSET * 2 + display.TEXT_SIZE)));

  //Changes Menu Option
  Vector2 analogInput = controller.getAnalogKey();
  if (analogInput.X == 1)
  {
    sound.playSound(NOTE_G5);
    changeSelection(1, 3);
  }
  else if (analogInput.X == -1)
  {
    sound.playSound(NOTE_G5);
    changeSelection(-1, 3);
  }

  //Confirms Selected Menu Option
  if (controller.getKey(KeyCode::A))
  {
    sound.playSound(NOTE_G5);
    switch (optionSelector)
    {
    case 0:
      cpu.level = 1;
      break;
    case 1:
      cpu.level = 4;
      break;
    case 2:
      cpu.level = 7;
      break;
    }
    difficultyScene = false;
    gameSetupScene = true;
    optionSelector = 0;
  }

  //Returns to Previous Scene
  if (controller.getKey(KeyCode::B))
  {
    sound.playSound(NOTE_G5);
    difficultyScene = false;
    menuScene = true;
    optionSelector = 0;
  }

  delay(10);
}

//Simulates a Loading Game
void displayLoading()
{
  sound.stop(); //Stops music
  tm1638.getModule().clearDisplay(); //Prepares 7-Seg to display match time
  musicFlag = false; // Doesnt let music to play

  //Menu tittle
  String message = "Loading";

  //Animation Logic for Loading Message
  for (int i = 0; i < 10; i++)
  {
    display.clearScreen();

    switch (optionSelector)
    {
    case 1:
      message += ".";
      break;
    case 2:
      message += "..";
      break;
    case 3:
      message += "...";
      break;

    default:
      message = "Loading";
      break;
    }
    display.writeText(message, Vector2(display.center.X - (message.length() * display.TEXT_SIZE / 2), display.center.Y - display.TEXT_SIZE));
    display.renderScreen();
    delay(500);
    changeSelection(1, 4);
  }

  //Loads next Scene
  gameSetupScene = false;
  gameScene = true;

  //Game Setup Variables
  matchStart = rtc.getTime(t); //Get Match starting time

  //Resets game objects to default vailes
  playerScore = 0;
  playerPaddle.position = Vector2(5, gameField.center.Y - playerPaddle.size.Y / 2);

  computerScore = 0;
  computerPaddle.position = Vector2(display.WIDTH - 5, gameField.center.Y - computerPaddle.size.Y / 2);

  ball.position = gameField.center;
  ball.direction.X = -1 * ball.speed;
}

//Displays End Menu Scene
void displayEndMenu()
{
  //Menu Tittle
  String message;
  if (playerWon)
  {
    message = "YOU WON\n\nCONGRATULATIONS!\n\nPress B to return";
  }
  else
  {
    message = "YOU LOST\n\nDon't give up!\n\nPress B to return";
  }
  display.writeText(message, Vector2(0, 0));

  //Button to return to Main Menu
  if(controller.getKey(KeyCode::B)){
    endScene = false;
    introScene = true;
  }
}
/* #endregion */

/* #region Game Logic */

// Player Movement Logic
void ProcessPlayer()
{
  Vector2 analogValue = controller.getAnalogKey(); // Gets the direction of the Analog Pressed
  if (analogValue.Y != 0) //Confirms its only on the Y Axis
  {
    playerPaddle.position.Y += analogValue.Y * playerPaddle.speed; //Moves the paddle to a new position
    playerPaddle.position.Y = Mathf::clamp(playerPaddle.position.Y, display.BLUE_OFFSET + 2, display.HEIGHT - playerPaddle.size.Y - 2); // checks if the Paddle is inside field, force it
  }
}

// CPU Movement Logic
void ProcessCPUMovement()
{
  if (cpu.getReaction() < cpu.reactionSucess) //Checks if CPU has reaction to ball
  {
    if ((computerPaddle.position.Y + (computerPaddle.position.Y + computerPaddle.size.Y)) / 2 > ball.position.Y) //Checks if ball is upwards related to paddle
    {
      computerPaddle.position.Y -= computerPaddle.speed; //Move paddle upwards
    }
    else if ((computerPaddle.position.Y + (computerPaddle.position.Y + computerPaddle.size.Y)) / 2 < ball.position.Y) //Checks if ball is bellow related to paddle
    {
        computerPaddle.position.Y += computerPaddle.speed; //Move paddle downwards
    }

    computerPaddle.position.Y = Mathf::clamp(computerPaddle.position.Y, display.BLUE_OFFSET + 2, display.HEIGHT - computerPaddle.size.Y - 2); // Makes CPU Paddle position be rendered inside fiels
  }
}

// CPU Reaction Logic
void ProcessCPU()
{
  if (!cpu.midfieldLookoutFlag && ball.position.X > display.WIDTH / 2) //Makes CPU React only when the ball is on its own midfield part
  {
    ProcessCPUMovement();
  }
  else if (cpu.midfieldLookoutFlag) //CPU is always tracking Ball
  {
    ProcessCPUMovement();
  }
}

// Ball Movement Logic
void ProcessBall()
{
  //ball hits player paddle
  if (ball.position.X - ball.size > 0 && ball.position.X - ball.size < playerPaddle.position.X + ball.size &&
      ball.position.Y >= playerPaddle.position.Y && ball.position.Y <= playerPaddle.position.Y + playerPaddle.size.Y)
  {
    ball.direction.X = 1 * ball.speed;

    int paddleParts = playerPaddle.size.Y / 3;

    if (ball.position.Y >= playerPaddle.position.Y + paddleParts * 2) //If touches the downwards part of paddle, go down
    {
      ball.direction.Y = 1 * ball.speed;
    }
    else if (ball.position.Y > playerPaddle.position.Y + paddleParts) //If touches the middle part of paddle, go straigthened
    {
      ball.direction.Y = 0;
    }
    else //If touches the upwards part of paddle, go up
    {
      ball.direction.Y = -1 * ball.speed;
    }

    sound.playSound(NOTE_G4);
  }

  //ball hits computer paddle
  if (ball.position.X + ball.size < display.WIDTH && ball.position.X + ball.size > computerPaddle.position.X - ball.size && ball.position.Y >= computerPaddle.position.Y && ball.position.Y <= computerPaddle.position.Y + computerPaddle.size.Y)
  {
    ball.direction.X = -1 * ball.speed;

    int paddleParts = computerPaddle.size.Y / 3;

    if (ball.position.Y >= computerPaddle.position.Y + paddleParts * 2) //If touches the downwards part of paddle, go down
    {
      ball.direction.Y = 1 * ball.speed;
    }
    else if (ball.position.Y > computerPaddle.position.Y + paddleParts) //If touches the middle part of paddle, go straigthened
    {
      ball.direction.Y = 0;
    }
    else //If touches the upwards part of paddle, go up
    {
      ball.direction.Y = -1 * ball.speed;
    }

    sound.playSound(NOTE_G4);
  }

  //If Ball hits top or bottom border
  if (ball.position.Y - ball.size < display.BLUE_OFFSET + ball.size)
  {
    ball.direction.Y = -ball.direction.Y;

    sound.playSound(NOTE_G4);
  }
  if (ball.position.Y + ball.size > display.HEIGHT - 1 - ball.size)
  {
    ball.direction.Y = -ball.direction.Y;

    sound.playSound(NOTE_G4);
  }

  ball.position.X += ball.direction.X;
  ball.position.Y += ball.direction.Y;

  //If ball passes Player Goal Line
  if (ball.position.X <= 0)
  {
    cpu.decreaseDifficulty(); //Changes CPU Difficulty

    computerScore++; //Updates Score

    //Resets ball Position
    ball.position = gameField.center;
    ball.direction = Vector2::left();
    ball.direction.X *= ball.speed;

    sound.playSound(NOTE_F1);
  }

  //If ball passes CPU Goal Line
  if (ball.position.X >= display.WIDTH)
  {
    cpu.increaseDifficulty(); //Changes CPU Difficulty

    playerScore++;//Updates Score

    //Resets ball Position
    ball.position = gameField.center;
    ball.direction = Vector2::right();
    ball.direction.X *= ball.speed;

    sound.playSound(NOTE_D5);
  }
}

//Renders the Player/CPU Name
void renderPlayerNames()
{
  display.writeText(PlayerPrefs::getUsername(), Vector2(0, display.BLUE_OFFSET / 2 - 2)); //Player Name
  display.writeText("vs", Vector2(display.WIDTH / 2 - 5, display.BLUE_OFFSET / 2 - 2));
  display.writeText("CPU", Vector2(display.WIDTH - (4 * 5), display.BLUE_OFFSET / 2 - 2)); //CPU Name
}

//Updates the Leds based on Scores
void UpdateLeds()
{
  int binaryComputer = 0;
  int binaryPlayer = 0;

  //Converts player Score to binary
  switch (playerScore)
  {
  case 1:
    binaryPlayer = 0b0001;
    break;
  case 2:
    binaryPlayer = 0b0011;
    break;
  case 3:
    binaryPlayer = 0b0111;
    break;
  case 4:
    binaryPlayer = 0b1111;
    break;
    case 5:
    binaryPlayer = 0b1111;
    break;
  }

  //Converts cpu score to binary
  switch (computerScore)
  {
  case 1:
    binaryComputer = 0b10000000;
    break;
  case 2:
    binaryComputer = 0b11000000;
    break;
  case 3:
    binaryComputer = 0b11100000;
    break;
  case 4:
    binaryComputer = 0b11110000;
    break;
    case 5:
    binaryComputer = 0b11110000;
    break;
  }

  tm1638.getModule().setLEDs(binaryPlayer + binaryComputer); // Sums the correct binary to be displayed and shows on LEDS
}

//Updates the Displays Numbers (7-Seg)
void UpdateDisplayNumbers()
{
  RifTime currentTime = rtc.getTime(t); // Gets Current Time

  //Calculates the diffrence of each time segment to know the tola time played
  int hoursDiff = currentTime.getHour() < matchStart.getHour() ? currentTime.getHour()+24 - matchStart.getHour() : currentTime.getHour() - matchStart.getHour();
  int minutesDiff = currentTime.getMinute() < matchStart.getMinute() ? currentTime.getMinute()+60 - matchStart.getMinute() : currentTime.getMinute() - matchStart.getMinute();
  int secondsDiff = currentTime.getSecond() < matchStart.getSecond() ? currentTime.getSecond()+60 - matchStart.getSecond() : currentTime.getSecond() - matchStart.getSecond();
  
  //Converts the time to Seconds
  tm1638.getModule().setDisplayToDecNumber(hoursDiff * 3600 + minutesDiff * 60 + secondsDiff,0);
}

// Renders the game objects to screen
void UpdateDisplay()
{
  ball.renderBall(display);
  playerPaddle.renderPaddle(display);
  computerPaddle.renderPaddle(display);
  gameField.renderField(display);

  renderPlayerNames();
  UpdateLeds();
  UpdateDisplayNumbers();
}

// The Game Loop
void gameLoop()
{

  ProcessPlayer();
  ProcessCPU();
  ProcessBall();

  UpdateDisplay();

  //Checks if the player or the cpu has reached the goal
  if (playerScore >= 5 || computerScore >= 5)
  {
    if (playerScore >= 5)
    {
      playerWon = true;
    }
    gameScene = false;
    endScene = true;
  }
}
/* #endregion */

// The Start Funtion
void setup()
{
  Serial.begin(115200);
  while (!Serial)
    ;
  PlayerPrefs::setUsername("RAY"); // Defines the Player Name

  display.copyrightIntro(); // Displays the legal part of the OLED hardware
}

// The Program Loop
void loop()
{
  //Resets sound and display to be reweitten
  sound.stop();
  display.clearScreen();

  if (endScene)
  {
    displayEndMenu();
  }

    if (gameScene)
  {
    gameLoop();
  }

  if (gameSetupScene)
  {
    displayLoading();
  }

  if (difficultyScene)
  {
    displayDiffucltyMenu();
  }

  if (menuScene)
  {
    displayMenu();
  }

  if (introScene)
  {
    displayIntro();
  }

  if(musicFlag){
    music.playSong();
  }

  display.renderScreen(); //renders final screen after everything
}