#include "Arduino.h"
#include "Pitches.h"
#include "Sound.h"
#include "MiiChannelSong.h"

//Constructors

//Default Values
Sound::Sound(){
    init(D0);
}

//Choose the PIN where the buzzer is connected
Sound::Sound(int buzzerPin){
    init(buzzerPin);
}

//Commun contructor method
void Sound::init(int buzzerPin){
    this->BUZZER = buzzerPin;
}

//Note to play
void Sound::playSound(int note){
    tone(BUZZER,note);
}

//Stop any Note playing
void Sound::stop(){
    noTone(BUZZER);
}