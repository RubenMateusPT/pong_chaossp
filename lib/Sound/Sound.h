#ifndef Sound_h
#define Sound_h

#include "Arduino.h"
#include "Pitches.h"

class Sound
{
    public:
    //Constructors
    Sound();
    Sound(int buzzerPin);

    //Methods
    void playSound(int note);
    void stop();

    private:
    void init(int buzzerPin);

    //Variables
    int BUZZER;
};

#endif