#ifndef MiiChannelSong_h
#define MiiChannelSong_h

#include "Arduino.h"
#include "Pitches.h"

class MiiChannelSong
{
    public:
    //Contructors
    MiiChannelSong();
    MiiChannelSong(int buzzerPin);

    //Methods
    void resetSong();
    void playSong();

    private:
    void init(int buzzerPin);

    //Variables
    int BUZZER_PIN;
};

#endif