#ifndef PlayerPrefs_h
#define PlayerPrefs_h

#include "Arduino.h"

class PlayerPrefs
{
    public:

    static String getUsername();
    static void setUsername(String username);

    private:
    static String username;
};

#endif