#ifndef Vector2_h
#define Vector2_h

class Vector2
{
    public:

    //Contructor
    Vector2();
    Vector2(int x, int y);

    //Properties
    int X;
    int Y;

    //Methods
    bool insideRange(int valueToCheck);

    //Static Methods
    static Vector2 up();
    static Vector2 left();
    static Vector2 right();
    static Vector2 down();

    private:
    void init(int x, int y);
};

#endif