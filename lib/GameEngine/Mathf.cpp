#include "Mathf.h"

//Different Methods to find and return the center beetween 2 points, 2 vectors or the center of a single vector
int Mathf::findCenter(int x, int y){
    return ((x+y)/2);
}

int Mathf::findCenter(Vector2 vector){
    return findCenter(vector.X, vector.Y);
}

Vector2 Mathf::findCenter(int x1, int y1, int x2, int y2){
    return Vector2(
        findCenter(x1,x2),
        findCenter(y1,y2)
    );
}

Vector2 Mathf::findCenter(Vector2 position, Vector2 size){
    return findCenter(position.X, position.Y, size.X, size.Y);
}

//Restrict value to be beetween a minimum and a maximum
int Mathf::clamp(int value, int min, int max){
    if(value <= min){
        return min;
    }
    
    if(value >= max){
        return max;
    }

    return value;
}