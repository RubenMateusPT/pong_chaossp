#include "Vector2.h"

//Contructors

//Defalut Values
Vector2::Vector2(){
    init(0,0);
}

//Assigned Values
Vector2::Vector2(int x, int y){
    init(x,y);
}

void Vector2::init(int x, int y){
    this->X = x;
    this->Y = y;
}

//Static Methods for quick access
Vector2 Vector2::up(){
    return Vector2(0,-1);
}

Vector2 Vector2::left(){
    return Vector2(-1,0);
}

Vector2 Vector2::right(){
    return Vector2(1,0);
}

Vector2 Vector2::down(){
    return Vector2(0,1);
}

//Checks if the value is inside the range X to Y
bool Vector2::insideRange(int valueToCheck){
    return valueToCheck >= X && valueToCheck <= Y ? true : false;
}