#ifndef Mathf_h
#define Mathf_h

#include "Vector2.h"

//Class based on Unity
class Mathf
{
    public:
    
    //Static Methods
    static int findCenter(int x, int y);
    static int findCenter(Vector2 vector);
    static Vector2 findCenter(int x1, int y1, int x2, int y2);
    static Vector2 findCenter(Vector2 position, Vector2 size);

    static int clamp(int value, int min, int max);
};

#endif