#include "Arduino.h"
#include "PlayerPrefs.h"

//Single Class to save on memory the name of the console user

String Username;

String PlayerPrefs::getUsername(){
    return Username;
}

void PlayerPrefs::setUsername(String username){
    Username = username;
}