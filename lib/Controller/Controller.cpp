#include "Arduino.h"
#include "Controller.h"
#include "Vector2.h"

//Constructors
//Default Values
Controller::Controller()
{
    init(D6,
         D7,
         D8,
         A0,
         Vector2(700, 715),
         Vector2(520, 530),
         Vector2(180, 190),
         Vector2(345, 355),
         Vector2(725, 745),
         Vector2(750, 770),
         Vector2(570, 590),
         Vector2(620, 640));
         }

//Assign the Pins of the Digital Buttons and Analog Pin, as well say the range of each directional button
Controller::Controller(int aButtonPin,
                       int bButtonPin,
                       int startButtonPin,
                       int analogPin,
                       Vector2 upButtonRange,
                       Vector2 downButtonRange,
                       Vector2 leftButtonRange,
                       Vector2 rightButtonRange,
                       Vector2 upLeftButtonRange,
                       Vector2 upRighttButtonRange,
                       Vector2 downLeftButtonRange,
                       Vector2 downRightButtonRange)
{
  init(aButtonPin,
       bButtonPin,
       startButtonPin,
       analogPin,
       upButtonRange,
       downButtonRange,
       leftButtonRange,
       rightButtonRange,
       upLeftButtonRange,
       upRighttButtonRange,
       downLeftButtonRange,
       downRightButtonRange);
}

//Common method for Constructors
void Controller::init(int aButtonPin,
                      int bButtonPin,
                      int startButtonPin,
                      int analogPin,
                      Vector2 upButtonRange,
                      Vector2 downButtonRange,
                      Vector2 leftButtonRange,
                      Vector2 rightButtonRange,
                      Vector2 upLeftButtonRange,
                      Vector2 upRighttButtonRange,
                      Vector2 downLeftButtonRange,
                      Vector2 downRightButtonRange)
{
  this->A_BUTTON = aButtonPin;
  this->B_BUTTON = bButtonPin;
  this->START_BUTTON = startButtonPin;

  this->ANALOG = analogPin;

  this->UP_BUTTON = upButtonRange;
  this->DOWN_BUTTON = downButtonRange;
  this->LEFT_BUTTON = leftButtonRange;
  this->RIGHT_BUTTON = rightButtonRange;

  this->UP_LEFT_BUTTON = upLeftButtonRange;
  this->UP_RIGHT_BUTTON = upRighttButtonRange;

  this->DOWN_LEFT_BUTTON = downLeftButtonRange;
  this->DOWN_RIGHT_BUTTON = downRightButtonRange;

  setupPins();
}

//Sets how the input pins should work
void Controller::setupPins()
{
  pinMode(A_BUTTON, INPUT_PULLUP);
  pinMode(B_BUTTON, INPUT_PULLUP);
  pinMode(START_BUTTON, INPUT_PULLUP);
}

//Checks if Button/Pin has been pressed
bool Controller::getKey(KeyCode code)
{
  switch (code)
  {
  case A:
    return !digitalRead(A_BUTTON) == 1;
    break;

  case B:
    return !digitalRead(B_BUTTON) == 1;
    break;

  case Start:
    return !digitalRead(START_BUTTON) == 1;
  }
  return false;
}

//Returns Direction based on Analog Values for Directional Buttons (8-Button Joystick)
Vector2 Controller::getAnalogKey()
{
  int value = analogRead(ANALOG);

  if (UP_BUTTON.insideRange(value))
  {
    return Vector2(0, -1);
  }
  else if (DOWN_BUTTON.insideRange(value))
  {
    return Vector2(0, 1);
  }
  else if (LEFT_BUTTON.insideRange(value))
  {
    return Vector2(-1, 0);
  }
  else if (RIGHT_BUTTON.insideRange(value))
  {
    return Vector2(1, 0);
  }
  else if (UP_LEFT_BUTTON.insideRange(value))
  {
    return Vector2(-1, -1);
  }
  else if (UP_RIGHT_BUTTON.insideRange(value))
  {
    return Vector2(1, -1);
  }
  else if (DOWN_LEFT_BUTTON.insideRange(value))
  {
    return Vector2(-1, 1);
  }
  else if (DOWN_RIGHT_BUTTON.insideRange(value))
  {
    return Vector2(1, 1);
  }

  return Vector2(0, 0);
}
