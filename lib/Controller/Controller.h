#ifndef Controller_h
#define Controller_h

#include "Arduino.h"
#include "Vector2.h"

//Digital Buttons
  enum KeyCode
  {
    A,
    B,
    Start
  };

class Controller
{

public:

  //Constructors
  Controller();
  Controller(int aButtonPin, 
              int bButtonPin, 
              int startButtonPin,
              int analogPin,
              Vector2 upButtonRange,
              Vector2 downButtonRange,
              Vector2 leftButtonRange,
              Vector2 rightButtonRange,
              Vector2 upLeftButtonRange,
              Vector2 upRighttButtonRange,
              Vector2 downLeftButtonRange,
              Vector2 downRightButtonRange);

  //Methods
  bool getKey(KeyCode key);
  Vector2 getAnalogKey();

private:
  //Variables
  int A_BUTTON;
  int B_BUTTON;
  int START_BUTTON;

  int ANALOG;
  Vector2  UP_BUTTON;
  Vector2  DOWN_BUTTON;
  Vector2  LEFT_BUTTON;
  Vector2  RIGHT_BUTTON;

  Vector2  UP_LEFT_BUTTON;
  Vector2  DOWN_LEFT_BUTTON;

  Vector2  UP_RIGHT_BUTTON;
  Vector2  DOWN_RIGHT_BUTTON;
  

  //Methods
  void init(int aButtonPin,
                       int bButtonPin,
                       int startButtonPin,
                       int analogPin,
                       Vector2 upButtonRange,
                       Vector2 downButtonRange,
                       Vector2 leftButtonRange,
                       Vector2 rightButtonRange,
                       Vector2 upLeftButtonRange,
                       Vector2 upRighttButtonRange,
                       Vector2 downLeftButtonRange,
                       Vector2 downRightButtonRange);
  void setupPins();
};

#endif