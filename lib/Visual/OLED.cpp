#include "Arduino.h"
#include "Streaming.h"
#include "Mathf.h"
#include "Vector2.h"

#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"

#include "OLED.h"

/* This Class works as a Wrapper for the SSD1306  for easier acces to create code*/

//Contructor
//Default Values
OLED::OLED(){
    display.begin(SSD1306_SWITCHCAPVCC, OLED_SCREEN_I2C_ADDRESS);
    WIDTH = display.width();
    HEIGHT = display.height();
    center = Mathf::findCenter(Vector2(0,0), Vector2(WIDTH,HEIGHT));
}

//Displays the obligatory copyright image
void OLED::copyrightIntro(){
    display.display();
    delay(2000);
    display.clearDisplay();
    display.setTextSize(1);
    display.setTextColor(WHITE);
    display.display();
}

//Clears the Screen
void OLED::clearScreen(){
    display.clearDisplay();
}

//Renders the screen with new information
void OLED::renderScreen(){
    display.display();
}

//Writes Text at position 0,0 of screen
void OLED::writeText(String message){
    writeText(message, Vector2(0,0));
}

//Writes text at selected position of screen
void OLED::writeText(String message, Vector2 position){
    display.setCursor(position.X, position.Y);
    display << message;
}

//Draws a line based on position and size
void OLED::drawLine(Vector2 position, Vector2 size){
    display.drawLine(position.X, position.Y, 
    position.X + size.X, position.Y + size.Y,
    WHITE);
}

//Draws a Rectangle based on position and size
void OLED::drawRect(Vector2 position, Vector2 size){
    display.drawRect(position.X, position.Y,
    size.X, size.Y,
    WHITE);
}

//Draws a Circle based on position and size
void OLED::drawCircle(Vector2 position, int size){
    display.fillCircle(position.X, position.Y, size, WHITE);
}

//Draws an Image based on position, size and source image
void OLED::drawBitmapImage(Vector2 position, Vector2 size, const unsigned char bitmapImage[]){
    display.drawBitmap(position.X, position.Y,
                        bitmapImage,
                        size.X, size.Y,
                        WHITE);
}