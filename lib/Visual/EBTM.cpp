#include "Arduino.h"
#include "TM1638.h"
#include "TM1638QYF.h"
#include "TM1640.h"
#include "TM16XX.h"
#include "TM16XXFonts.h"
#include "EBTM.h"

#define TM1638_STB D5
#define TM1638_CLK D4
#define TM1638_DIO D3

TM1638 module(TM1638_DIO, TM1638_CLK, TM1638_STB);

//This class serves as a Wrapper as to not repeat the same code

EBTM::EBTM(){
    module.clearDisplay();
    module.setupDisplay(true, 2);
}

TM1638 EBTM::getModule(){
    return module;
}