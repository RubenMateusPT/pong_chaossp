#ifndef OLED_h
#define OLED_h

#include "Arduino.h"
#include "Streaming.h"
#include "Vector2.h"

#include "Adafruit_GFX.h"
#include "Adafruit_SSD1306.h"

#define OLED_RESET -1
#define OLED_SCREEN_I2C_ADDRESS 0x3C

class OLED
{
    public:

    //Contants
    const int BLUE_OFFSET = 16;
    const int TEXT_SIZE = 6;

    //Variables
    int WIDTH;
    int HEIGHT;
    Vector2 center;

    //Contructor
    OLED();

    //Methods
    void copyrightIntro();

    void clearScreen();
    void renderScreen();

    void writeText(String message);
    void writeText(String message, Vector2 position);

    void drawLine(Vector2 position, Vector2 size);
    void drawRect(Vector2 position, Vector2 size);
    void drawCircle(Vector2 position, int size);
    void drawBitmapImage(Vector2 position, Vector2 size, const unsigned char bitmapImage[]);


    private:
    //Variables
    Adafruit_SSD1306 display;
};

#endif