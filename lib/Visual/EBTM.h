#ifndef EBTM_h
#define EBTM_h

#include "Arduino.h"
#include "TM1638.h"
#include "TM1638QYF.h"
#include "TM1640.h"
#include "TM16XX.h"
#include "TM16XXFonts.h"

class EBTM
{
    public:
    //Contructor
    EBTM();

    //Methods
    TM1638 getModule();
};

#endif