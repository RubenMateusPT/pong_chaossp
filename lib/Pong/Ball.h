#ifndef Ball_h
#define Ball_h

#include "Vector2.h"
#include "OLED.h"

class Ball
{
    public:
    //Contructos
    Ball();
    Ball(Vector2 position, Vector2 direction, int size, int speed);

    //Variables
    Vector2 position;
    Vector2 direction;
    int size;
    int speed;

    //Methods
    void renderBall(OLED display);
    
    private:
    void init(Vector2 position, Vector2 direction, int size, int speed);
};

#endif