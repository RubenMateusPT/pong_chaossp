#ifndef CPU_h
#define CPU_h

#include "Paddle.h"

class CPU
{
    public:

    //Contructors
    CPU(Paddle computerPaddle);
    CPU(Paddle computerPaddle, int level);
    
    //Variables
    Paddle computerPaddle;
    bool midfieldLookoutFlag;
    int level;
    int reactionSucess;

    //Methods
    int getReaction();
    void increaseDifficulty();
    void decreaseDifficulty();

    private:
    void init(Paddle computerPaddle, int level);

    //Constants
    const int MIN_LEVEL = 1, MAX_LEVEL = 9;

    //Methods
    void changeDifficulty();
};

#endif