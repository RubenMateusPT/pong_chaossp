#include "Vector2.h"
#include "OLED.h"
#include "Ball.h"

//Constructos

//Default Values
Ball::Ball(){
    init(Vector2(), Vector2(), 0, 0);
}

//Assing Values at Creation
Ball::Ball(Vector2 position, Vector2 direction, int size, int speed){
    init(position, direction, size, speed);
}

//Common Method for Contructor
void Ball::init(Vector2 position, Vector2 direction, int size, int speed){
    this->position = position;
    this->direction = direction;
    this->size = size;
    this->speed = speed;
}

//Fast Way to ask Screen to Render Ball
void Ball::renderBall(OLED visual){
    visual.drawCircle(this->position, this->size);
}