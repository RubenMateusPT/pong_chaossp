#ifndef Paddle_h
#define Paddle_h

#include "Vector2.h"
#include "OLED.h"

class Paddle
{
    public:
    //Constructors
    Paddle();
    Paddle(Vector2 position, Vector2 size, int speed);

    //Variables
    Vector2 position;
    Vector2 size;
    int speed;

    Vector2 center;

    //Methods
    void renderPaddle(OLED display);
    
    private:
    void init(Vector2 position, Vector2 size, int speed);
};

#endif