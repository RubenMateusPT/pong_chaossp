#include "Vector2.h"
#include "OLED.h"
#include "Paddle.h"
#include "Mathf.h"

//Contructors
//Default Values
Paddle::Paddle(){
    init(Vector2(), Vector2(), 0);
}

// Assign Values at creation
Paddle::Paddle(Vector2 position, Vector2 size, int speed){
    init(position, size, speed);
}

//Commun Method for Contructors
void Paddle::init(Vector2 position, Vector2 size, int speed){
    center = Mathf::findCenter(position,center);
    this->size = size;
    this->position = position;
    this->speed = speed;
}

// Fast Way to render Padlle on Screen
void Paddle::renderPaddle(OLED display){
    display.drawLine(position, size);
}