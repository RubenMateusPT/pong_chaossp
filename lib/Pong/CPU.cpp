#include "Paddle.h"
#include "Mathf.h"
#include "CPU.h"

//Contructors
//default Values
CPU::CPU(Paddle computerPaddle){
    init(computerPaddle, 2);
}

//Assigne Values At Creation
CPU::CPU(Paddle computerPaddle, int level){
    init(computerPaddle, level);
}

//Common Method for Contructors
void CPU::init(Paddle computerPaddle, int level){
    this->computerPaddle = computerPaddle;
    this->level = level;
    this->midfieldLookoutFlag = false;

    changeDifficulty();
}

//Return CPU Reaction Level to Ball
int CPU::getReaction(){
    return random(0,10);
}

// Increases the Difficulty Level of CPU (More Smart)
void CPU::increaseDifficulty(){
    this->level++;
    changeDifficulty();
}

// Decreases the Difficulty Level of CPU (Less Smart)
void CPU::decreaseDifficulty(){
    this->level--;
    changeDifficulty();
}

//Changes the CPU Level, by capping beetween Minimum and Maximum Level, and says how to react at certain Levels
void CPU::changeDifficulty(){
    this->level = Mathf::clamp(this->level, MIN_LEVEL, MAX_LEVEL);

    if(level > 5){
        midfieldLookoutFlag = true;
        reactionSucess = level - 5;
    }
    else {
        midfieldLookoutFlag = false;
        reactionSucess = level;
    }
}