#include "Field.h"
#include "Mathf.h"
#include "OLED.h"

//Contructors
//Default Values
Field::Field(){
    init(Vector2(), Vector2());
}

//Assign Values at Creation
Field::Field(Vector2 position, Vector2 size){
    init(position,size);
}

//Common Method for Contructors
void Field::init(Vector2 position, Vector2 size){
    center = Mathf::findCenter(position,size);
    this->position = position;
    size.Y -= position.Y;
    this->size = size;
}

//Fast way to ask display to render Field
void Field::renderField(OLED display){
    display.drawRect(position, size);
}