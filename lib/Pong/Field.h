#ifndef Field_h
#define Field_h

#include "Vector2.h"
#include "OLED.h"
#include "Mathf.h"

class Field
{
    public:
    //Contructors
    Field();
    Field(Vector2 position, Vector2 size);

    //Variables
    Vector2 position;
    Vector2 size;

    Vector2 center;

    //Methods
    void renderField(OLED display);
    
    private:
    void init(Vector2 position, Vector2 size);
};

#endif